# Changelog

## 1.2 (2023-08-20)

* Prevent editing sign with right click implemented with Minecraft 1.20
* Now requires at least Minecraft 1.20.1

## 1.1 (2020-10-21)

Implemented support for Dynmap markers

## 1.0 (2019-08-27)

Initial release